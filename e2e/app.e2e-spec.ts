import { AppPage } from './app.po';
import {browser, by, element} from 'protractor';

describe('angular4-poc App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to Angular 4 POC!');
  });

  it('should click login with google will open a popup', () => {
    browser.waitForAngularEnabled(false);
    page.navigateTo();
    const googleButton = element(by.css('button.zocial'));
    googleButton.click();
    const handlePromise = browser.getAllWindowHandles();
    handlePromise.then((handles) => {
      expect(handles.length).toEqual(2);
    });
  });
});
