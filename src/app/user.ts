export class User {
  gender: string;
  displayName: string;
  imageUrl: string;
  email: string;
  constructor(values) {
    this.gender = values.gender || '';
    this.displayName = values.displayName || '';
    this.imageUrl = values.image.url || '';
    this.email = values.email || '';
  }
}
