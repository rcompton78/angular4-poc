import { Injectable } from '@angular/core';

import * as hellojs from 'hellojs';
import {User} from './user';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class AuthService {
  GOOGLE_CLIENT_ID: String = '425070609502-t0pblnkgjj4hfn29igpjr5f6qjbpv579.apps.googleusercontent.com';
  private _user: ReplaySubject<User> = new ReplaySubject<User>();

  constructor() {
    hellojs.init(
      {
        google: this.GOOGLE_CLIENT_ID
      },
      {
        redirect_uri: 'http://localhost:4200/redirect.html'
      }
    );
  }

  doSocialLogin(idpName: string) {
    hellojs.login(idpName, {scope: 'basic,email'})
      .then(
        () => {
          hellojs(idpName).api('me')
            .then((r) => {
              this.setUser(new User(r));
            });
        }
      );
  }

  doSocialLogout(idpName: string) {
    this.setUser(null);
    hellojs.logout(idpName);
  }

  getUser(): Observable<User> {
    return this._user.asObservable();
   }

  setUser(user: User) {
    this._user.next(user);
  }
}
