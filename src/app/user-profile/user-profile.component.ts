import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {User} from '../user';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  @Input() public user: User;

  constructor() { }

  ngOnInit() {
  }

}
