import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {AuthService} from '../auth.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [ AuthService ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should call auth service do social login on do login', () => {
    const authService = fixture.debugElement.injector.get(AuthService);
    spyOn(authService, 'doSocialLogin');
    component.ngOnInit();
    component.doLogin('Google');
    expect(authService.doSocialLogin).toHaveBeenCalledWith('Google');
  });

  it('should call auth service do social logout on do logout', () => {
    const authService = fixture.debugElement.injector.get(AuthService);
    spyOn(authService, 'doSocialLogout');
    component.ngOnInit();
    component.doLogout();
    expect(authService.doSocialLogout).toHaveBeenCalled();
  });
});
