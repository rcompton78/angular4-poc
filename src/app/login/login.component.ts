import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {User} from '../user';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  idpName: string;
  user: User;
  subscription: Subscription;

  constructor(private authService: AuthService, private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.subscription = this.authService.getUser()
      .subscribe(
        (user: User) => {
          console.log('user in login', user);
          this.user = user;
          this.cd.detectChanges();
        }, error => console.error(error));
  }

  doLogin(idpName: string) {
    console.log('do login');
    this.idpName = idpName;
    this.authService.doSocialLogin(this.idpName);
  }

  doLogout() {
    this.authService.doSocialLogout(this.idpName)
    this.idpName = '';
  }
}
