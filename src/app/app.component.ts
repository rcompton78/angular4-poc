import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {User} from './user';
import {Subscription} from 'rxjs/Subscription';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title: string;
  subscription: Subscription;
  user: User;


  constructor(private authService: AuthService, private cd: ChangeDetectorRef) {
    this.title = 'Angular 4 POC';
  }

  ngOnInit() {
    this.subscription = this.authService.getUser()
      .subscribe(
        (user: User) => {
          this.user = user;
          this.cd.detectChanges();
        }, error => console.error(error));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
